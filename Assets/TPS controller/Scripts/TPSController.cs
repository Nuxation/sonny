﻿using UnityEngine;
using System.Collections;

public class TPSController : MonoBehaviour {
    private enum MovementState { Idle, Walking, Running, WalkingBack, StepRight, StepLeft };
    private readonly Vector3[] MOVEMENT_MASK_VECTOR = {
        new Vector3(0, 0, 0), // Idle
        new Vector3(0, 0, 1f), // Walking
        new Vector3(0, 0, 5f), // Running
        new Vector3(0, 0, 1f), // WalkingBack
        new Vector3(1f, 0, 0), // StepRight
        new Vector3(1f, 0, 0) // StepLeft
    };

    //This variable indicates how is the current state of character.
    private MovementState state;

    //Define the turning speed.
    public float angularVelocity = 4.0f;
    public float speed = 1.0f;
    
    // To command camera
    private float horizontal;
    private float vertical;
    private Animator animator;

    //This variable indicates if the player is aiming or not.
    private bool aiming; 
    private bool shiftPressed = false;

    //Get the camera properties.
    public Camera camera;
    public GameObject OrbitalCamera;

    void Start ()
    {
        animator = GetComponentInChildren<Animator>();
        state = MovementState.Idle;
        aiming = false;
        horizontal = transform.eulerAngles.y;        
        vertical = transform.eulerAngles.x;

        // Lock the cursor on the screen and hide it
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    void Update ()
    {
        UpdateMovement();
        animator.SetInteger("State", (int)state);
        UpdateCamera();
    }

    private void UpdateMovement()
    {
        Vector3 moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
        
        // Moving forward is priorize
        bool isStepMovement = Mathf.Abs(moveDirection.x) > Mathf.Abs(moveDirection.z);
        bool isMove = Mathf.Abs(moveDirection.x) > 0 || Mathf.Abs(moveDirection.z) > 0;
        bool isBackMove = moveDirection.z < 0;

        // Check if the user press shift
        if(Input.GetKeyDown(KeyCode.LeftShift)) {
            shiftPressed = true; // Remember that the key is pressed
        }
        // Check if he release the button
        else if(Input.GetKeyUp(KeyCode.LeftShift)) {
            shiftPressed = false;
        }

        // Detect if the state must be changed
        if(isMove && !isStepMovement && !isBackMove && !shiftPressed) {
            state = MovementState.Walking;
        }
        else if(isMove && !isStepMovement && !isBackMove && shiftPressed) {
            state = MovementState.Running;
        }
        else if(isMove && isBackMove && !isStepMovement) {
            state = MovementState.WalkingBack;
        }
        else if(isMove && isStepMovement && moveDirection.x > 0) {
            state = MovementState.StepRight;
        }
        else if(isMove && isStepMovement && moveDirection.x < 0) {
            state = MovementState.StepLeft;
        }
        else if(!isMove) {
            state = MovementState.Idle;
        }

        // Check if the user click on the left click
        if (Input.GetKeyDown(KeyCode.Mouse1)) {
            aiming = true; // Remember that the key is pressed
        // Check if he release the button
        } else if (Input.GetKeyUp(KeyCode.Mouse1)) { 
            aiming = false; 
        }

        // If the user want to run but is aiming, force walking state
        if(aiming && state == MovementState.Running) {
            state = MovementState.Walking;
        }

        // Fix the movement via the state, apply mask
        moveDirection = Vector3.Scale(moveDirection, MOVEMENT_MASK_VECTOR[(int)state]);
        moveDirection *= speed;

        // Apply movement
        transform.Translate(moveDirection * Time.deltaTime);

        // When the player move his mouse horizontally, the whole character move
        horizontal = (horizontal + angularVelocity * Input.GetAxis("Mouse X")) % 360f;
        transform.rotation = Quaternion.AngleAxis(horizontal, Vector3.up);
        
        // Update camera vertical axis
        vertical = (vertical - angularVelocity * Input.GetAxis("Mouse Y")) % 360f;
        vertical = Mathf.Clamp(vertical, -30, 60); // Min and max 
        OrbitalCamera.transform.localRotation = Quaternion.AngleAxis(vertical, Vector3.right);
    }

    private void UpdateCamera()
    {
        // Zoom if the user is aiming
        if (aiming == true && camera.fieldOfView > 37) {
            camera.fieldOfView = camera.fieldOfView - 65.0f * Time.deltaTime;
        }
        if (aiming == false && camera.fieldOfView < 60) {
            camera.fieldOfView = camera.fieldOfView + 65.0f * Time.deltaTime;
        }
    }
}
