using System.Collections;
using System.Collections.Generic;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UIElements;

public class PlayerControler : MonoBehaviour
{
    public float walkSpeed = 8f;
    public float jumpSpeed = 7f;
    private float _cameraSpeed = 4f;

    private Rigidbody _rb;
    private Collider _coll;
    private Animator _anim;
    private Transform _transform;

    private float _jump_timer;


    public int score = 0;
    public Text textScore;


    //flag to keep track of whether a jump started
    private bool _pressedJump = false;

    // Start is called before the first frame update
    void Start()
    {
        textScore.text = "Collecter les croquettes";
        _rb = GetComponent<Rigidbody>();
        _coll = GetComponent<Collider>();
        _anim = gameObject.GetComponent<Animator>();
        _transform = transform;
        AnimChangeState("Idle");
    }

    // Update is called once per frame
    void Update()
    {
        // Handle player walking
        WalkHandler();

        //Handle player jumping
        JumpHandler();

        RotationHandler();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Croquette")
        {
            score++;
           
            if (score == 20)
            {
                textScore.text = "Bravo, tu as gagné !";
            } else
            {
                textScore.text = "Score: " + score;
            }


            Debug.Log(score);
        }

    }

    // Make the player walk according to user input
    void WalkHandler()
    {
        // Set x and z velocities to zero
        _rb.velocity = new Vector3(0, _rb.velocity.y, 0);
        // Distance ( speed = distance / time --> distance = speed * time)
        float distance = walkSpeed * Time.deltaTime;
        // Input on x ("Horizontal")
        float xAxis = Input.GetAxis("Horizontal");
        // Input on z ("Vertical")
        float zAxis = Input.GetAxis("Vertical");

        if (xAxis != 0 || zAxis != 0)
        {
            AnimChangeState("Run");
        }
        else
        {
            AnimChangeState("stop");
        }


        // Movement vector
        Vector3 movement = new Vector3(xAxis * distance, 0f, zAxis * distance);
        // Current position
        Vector3 currPosition = _transform.position;
        // New position
        Vector3 newPosition = currPosition + _transform.rotation * movement;
        // Move the rigid body
        _rb.MovePosition(newPosition);
    }

    void RotationHandler()
    {
        transform.Rotate(new Vector3(0, Input.GetAxis("Mouse X") * _cameraSpeed, 0));
        /*
        // Input on x ("Horizontal")
        float xAxis = Input.GetAxis("Horizontal");
        // Input on z ("Vertical")
        float zAxis = Input.GetAxis("Vertical");
        var rotationVector = transform.rotation.eulerAngles;

        if (xAxis == 0 && zAxis > 0)
        {
            rotationVector.y = 0;
        }
        else if (xAxis > 0 && zAxis > 0)
        {
            rotationVector.y = 45;
        }
        else if (xAxis > 0 && zAxis == 0)
        {
            rotationVector.y = 90;
        }
        else if (xAxis > 0 && zAxis < 0)
        {
            rotationVector.y = 135;
        }
        else if (xAxis == 0 && zAxis < 0)
        {
            rotationVector.y = 180;
        }
        else if (xAxis < 0 && zAxis < 0)
        {
            rotationVector.y = 225; // Ici
        }
        else if (xAxis < 0 && zAxis == 0)
        {
            rotationVector.y = 270;
        }
        else if (xAxis < 0 && zAxis > 0)
        {
            rotationVector.y = 315;
        }

        transform.rotation = Quaternion.Euler(rotationVector);*/
    }

    void JumpHandler()
    {
        // Jump axis
        float jAxis = Input.GetAxis("Jump");

        if (_jump_timer > 0)
        {
            _jump_timer = _jump_timer - Time.deltaTime;
            if (_jump_timer < 0)
                _jump_timer = 0;
        }

        // Is grounded
        bool isGrounded = CheckGrounded();

        if (jAxis > 0f)
        {
            if (!_pressedJump && isGrounded || _jump_timer == 0) //Retiré le true quand condition réparée
            {
                // We are jumping on the current key press
                _pressedJump = true;
                _jump_timer = 1.5f;

                // Jumping vector
                Vector3 jumpVector = new Vector3(0f, jumpSpeed, 0f);
                // Make the player jump by adding velocity
                _rb.velocity += jumpVector;
            }
        }
        else
        {
            // Update flag so it can jump again if we press the jump key
            _pressedJump = false;
        }

        bool CheckGrounded()
        {
            // Object size in x
            var bounds = _coll.bounds;
            float sizeX = bounds.size.x;
            float sizeZ = bounds.size.z;
            float sizeY = bounds.size.y;
            // Position of the 4 bottom corners of the game object
            // We add 0.01 in Y so that there is some distance between the point and the floor
            var position = transform.position;
            Vector3 corner1 = position + new Vector3(sizeX / 2, -sizeY / 2 + 0.01f, sizeZ / 2);
            Vector3 corner2 = position + new Vector3(-sizeX / 2, -sizeY / 2 + 0.01f, sizeZ / 2);
            Vector3 corner3 = position + new Vector3(sizeX / 2, -sizeY / 2 + 0.01f, -sizeZ / 2);
            Vector3 corner4 = position + new Vector3(-sizeX / 2, -sizeY / 2 + 0.01f, -sizeZ / 2);
            // Send a short ray down the cube on all 4 corners to detect ground
            bool grounded1 = Physics.Raycast(corner1, new Vector3(0, -1, 0), 0.01f);
            bool grounded2 = Physics.Raycast(corner2, new Vector3(0, -1, 0), 0.01f);
            bool grounded3 = Physics.Raycast(corner3, new Vector3(0, -1, 0), 0.01f);
            bool grounded4 = Physics.Raycast(corner4, new Vector3(0, -1, 0), 0.01f);
            // If any corner is grounded, the object is grounded
            return (grounded1 || grounded2 || grounded3 || grounded4);
        }
    }


    private void AnimChangeState(string animationName)
    {
        _anim.SetBool("Walk", false);
        _anim.SetBool("Jump", false);
        _anim.SetBool("Run", false);

        switch (animationName)
        {
            case "Walk":
                _anim.SetBool("Walk", true);
                break;
            case "Run":
                _anim.SetBool("Run", true);
                break;
            case "Jump":
                _anim.SetBool("Jump", true);
                break;
            case "Idle":
                _anim.SetTrigger("Idle");
                break;
            default:
                break;
        }
    }
}