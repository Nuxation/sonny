using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Croquette : MonoBehaviour
{
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Chat")
        {
            Destroy(this.gameObject);
        }

    }
}
