using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Transform target;
    private float _cameraSpeed = 4f;
    private Transform _transform;

    public Vector3 offset;

    // Start is called before the first frame update
    void Start()
    {
        _transform = transform;
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    void OnGUI()
    {

    }

    // Update is called once per frame
    void Update()
    {
        //transform.position = target.position + offset;
    }

    private void LateUpdate()
    {
        Rotate();
        transform.position = Vector3.Lerp(transform.position, target.position + offset, .25f);
    }

    private void Rotate()
    {
        offset = Quaternion.AngleAxis(Input.GetAxis("Mouse X") * _cameraSpeed, Vector3.up) * offset;
        _transform.LookAt(target);
    }
}